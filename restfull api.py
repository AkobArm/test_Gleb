from fastapi import FastAPI
from pydantic import BaseModel
from typing import List
import asyncio
import psycopg2
from random import choice

app = FastAPI()

class Item(BaseModel):
    id: int
    name: str
    gender: str

@app.post("/add_data")
async def add_data(item: Item):
    conn = psycopg2.connect(
        host="localhost",
        database="test_db",
        user="postgres",
        password="postgres"
    )
    cursor = conn.cursor()

    if isinstance(item.id, int) and isinstance(item.name, str) and isinstance(item.gender, str):
        cursor.execute(f"INSERT INTO test_table (id, name, gender) VALUES ({item.id}, '{item.name}', '{item.gender}')")
        conn.commit()
        return {"message": "Data added successfully"}
    else:
        return {"message": "Invalid data"}

@app.get("/get_data")
async def get_data(gender: str = None, limit: int = 100):
    conn = psycopg2.connect(
        host="localhost",
        database="test_db",
        user="postgres",
        password="postgres"
    )
    cursor = conn.cursor()

    if gender:
        cursor.execute(f"SELECT * FROM test_table WHERE gender='{gender}' LIMIT {limit}")
    else:
        cursor.execute(f"SELECT * FROM test_table LIMIT {limit}")

    rows = cursor.fetchall()
    data = []
    for row in rows:
        data.append({"id": row[0], "name": row[1], "gender": row[2]})

    return data

@app.get("/fill_data")
async def fill_data():
    conn = psycopg2.connect(
        host="localhost",
        database="test_db",
        user="postgres",
        password="postgres"
    )
    cursor = conn.cursor()

    for i in range(1000):
        name = ''.join(choice("abcdefghijklmnopqrstuvwxyz") for _ in range(10))
        gender = choice(["male", "female"])
        cursor.execute(f"INSERT INTO test_table (id, name, gender) VALUES ({i}, '{name}', '{gender}')")
    conn.commit()

    return {"message": "Data filled successfully"}
